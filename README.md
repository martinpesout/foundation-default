## Nedercare

### Deployment

* `yum install yum install libpng-devel.x86_64`
* `npm install bower -g`
* `npm install` - install node.js modules
* `bower install` - run bower
* `grunt` - run grunt tasks

### Grunt tasks

* `grunt` - run development release of the web, open it in a new browser window with live reload
* `grunt publish` - publish staging release of the web with minimized CSS and JS files
