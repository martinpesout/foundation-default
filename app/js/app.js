'use strict';

var app = (function(document, $) {
	var docElem = document.documentElement,
		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		_scrollingLinkInit = function() {
			/* scrolling effect to specific anchor on the page marked by #hash */
			$('.js-scrolling-link').click(function() {
				if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') || location.hostname === this.hostname) {

					var target = $(this.hash),
					position = 0;

					if (this.hash.slice(1).length > 0) {
						target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
						if (target.length) {
							position = target.offset().top - 60;
						}
					}

					$('html,body').animate({
						scrollTop: position
					}, 1000);
					return false;
				}
			});
		},
		_init = function() {
			$(document).foundation();
            // needed to use joyride
            // doc: http://foundation.zurb.com/docs/components/joyride.html
            $(document).on('click', '#start-jr', function () {
                $(document).foundation('joyride', 'start');
            });
			_userAgentInit();
			_scrollingLinkInit();
		};
	return {
		init: _init
	};
})(document, jQuery);

(function() {
	app.init();
})();
